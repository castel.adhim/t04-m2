import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try{
            //Input untuk bilangan pembilang
            System.out.print("Masukkan angka Pembilang = ");
            int Pem = scanner.nextInt();
            //Input untuk bilangan penyebut
            System.out.println("Masukkan angka Penyebut = ");
            int Pen = scanner.nextInt();
            //method untuk menghilang bilangan yang sudah di input
            int hasil = pembagian(Pem,Pen);
            
            //hasil akah di keluar kan melalui output ini dan boolean valid input menjadi true
            System.out.println("Hasil Akhir Adalah " + hasil);
            validInput = true;
            
            //dibawah ini adalah catch untuk Mengecek Kategori yang diberikan
            }catch(InputMismatchException e){
                System.out.println("Angka yang dimasukkan tidak boleh angka bulat dan angka desimal");
                scanner.nextLine();
            }catch(ArithmeticException e){
                System.out.println("Angka yang dimasukkan tidak boleh angka bulat dan angka desimal");
                scanner.nextLine();
            }
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0){
            throw new ArithmeticException("Penyebut tidak boleh bernilai 0");
        }
        return pembilang / penyebut;
    }
}
